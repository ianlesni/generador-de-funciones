/*
===============================================================================
 Name        : Prueba_ENCODER.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/
#include "chip.h"
#define TICKRATE_HZ 		1000 /* 1000 ticks per second */
bool encoder_CLK_last ;
bool encoder_DT_last;
bool encoder_CLK = false;
bool encoder_DT = false;
int encoder_position = 0 , a=0 , b=0 , i=0;

static uint32_t tick_ct = 0;
void SysTick_Handler(void)
{
	tick_ct += 1;
}

void GPIO3_IRQHandler(void)
{
	if(encoder_DT == false)
	{encoder_CLK = true;}
	if(encoder_position<0 || encoder_position> 2)
	{encoder_position = 0;}
Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH3);
}

void GPIO5_IRQHandler(void)
{
	if( encoder_CLK == true)
	{
	 encoder_position--;
	 encoder_CLK = false;
	}
	else
	{
		encoder_position++;
		encoder_DT = true;
	}
	if(encoder_position<0 || encoder_position> 2)
	{encoder_position = 0;}
Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH5);
}


int main(void)
{
	SystemCoreClockUpdate();
	Chip_GPIO_Init(LPC_GPIO_PORT);
	Chip_SCU_PinMux( 6 , 9 ,  SCU_MODE_INACT   | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );//encoder_CLK
	Chip_SCU_PinMux( 6 , 10 , SCU_MODE_INACT   | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );//encoder_SW
	Chip_SCU_PinMux( 6 , 8 , SCU_MODE_INACT   | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//encoder_DT

	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 3, 5); //encoder_CLK
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 3, 6); //encoder_SW
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 5, 16); //encoder_DT

	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 0, 14); //LED1
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 1, 11); //LED2
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 1, 12); //LED3

	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) false); //LED1
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool) false); //LED2
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool) false); //LED3

	Chip_SCU_PinMux( 2 , 0 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//LED0_R
	Chip_SCU_PinMux( 2 , 1 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//LED0_G
	Chip_SCU_PinMux( 2 , 2 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//LED0_B

	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 0); //LED0_R
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 1); //LED0_G
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 2); //LED0_B

	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 0, (bool) false); //LED0_R
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 1, (bool) false); //LED0_G
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) false); //LED0_B

	SysTick_Config(SystemCoreClock / TICKRATE_HZ);	//204.000 ticks entre interrupciones -> interrumpe cada 100ms

	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH3);
	Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH3);
	Chip_PININT_EnableIntLow(LPC_GPIO_PIN_INT, PININTCH3);

	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH5);
	Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH5);
	Chip_PININT_EnableIntLow(LPC_GPIO_PIN_INT, PININTCH5);

	Chip_SCU_GPIOIntPinSel(3, 3, 5);
	Chip_SCU_GPIOIntPinSel(5, 5, 16);

	NVIC_ClearPendingIRQ(PIN_INT3_IRQn );
	NVIC_EnableIRQ(PIN_INT3_IRQn);

	NVIC_ClearPendingIRQ(PIN_INT5_IRQn );
	NVIC_EnableIRQ(PIN_INT5_IRQn);



	while(1)
	{

		switch(encoder_position)
		{
			case 0:
				Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool) false);
				Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool) false);
				Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) true);
				b=0;
				break;
			case 1:
				Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) false);
				Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool) false);
				Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool)  true);
				b=1;
				break;
			case 2:
				Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) false);
				Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool) false);
				Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool)  true);
				b=2;
				break;
			default:
				Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) false);
				Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool) false);
				Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool) false);
				break;
		}

		 if(Chip_GPIO_GetPinState(LPC_GPIO_PORT, 3, 6) == 0)
			   {
				switch(b)
				{
					case 0:
						Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 0, (bool) true); //LED0_R
						Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 1, (bool) false); //LED0_G
						Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) false); //LED0_B
							break;
					case 1:
						Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 0, (bool) false); //LED0_R
					    Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 1, (bool) false); //LED0_G
					    Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) true); //LED0_B
							break;
					case 2:

						Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 0, (bool) false); //LED0_R
						Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 1, (bool) true); //LED0_G
						Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) false); //LED0_B
							break;
				}
			}
	}


}
