#include "chip.h"
#include "math.h"
#define LPC_SSP    LPC_SSP1
#define TICKRATE_HZ 		1000 /* 1000 ticks per second */

long int counter= 0;
long int function = 0;

static uint32_t tick_ct = 0;
void SysTick_Handler(void)
{
	tick_ct += 1;
}

void WriteRegister(int dat)
{
	int i=0;
    //SPI.setDataMode(SPI_MODE2);
	Chip_SSP_Disable(LPC_SSP);
	Chip_SSP_Init(LPC_SSP);
	Chip_SSP_SetFormat(LPC_SSP, SSP_BITS_8,  SSP_FRAMEFORMAT_SPI, SSP_CLOCK_MODE2);
	Chip_SSP_Set_Mode(LPC_SSP, SSP_MODE_MASTER);
	Chip_SSP_SetClockRate(LPC_SSP, 1, 1);
	Chip_SSP_Enable(LPC_SSP);

   //digitalWrite(FSYNC, LOW);
   Chip_GPIO_SetPinState(LPC_GPIO_PORT, 3, 0, (bool) false);

  // delayMicroseconds(10);
   for(i=0;i<1000;i++);

   //SPItransfer(dat>>8);
   Chip_SSP_SendFrame(LPC_SSP, dat>>8);

   //SPItransfer(dat&0xFF);
   Chip_SSP_SendFrame(LPC_SSP, dat&0xFF);

   //digitalWrite(FSYNC, HIGH);
   Chip_GPIO_SetPinState(LPC_GPIO_PORT, 3, 0, (bool) true); //Write done.Set FSYNC high


   //SPI.setDataMode(SPI_MODE0);
   Chip_SSP_Disable(LPC_SSP);
   Chip_SSP_Init(LPC_SSP);
   Chip_SSP_SetFormat(LPC_SSP, SSP_BITS_8,  SSP_FRAMEFORMAT_SPI, SSP_CLOCK_MODE0);
   Chip_SSP_Set_Mode(LPC_SSP, SSP_MODE_MASTER);
   Chip_SSP_SetClockRate(LPC_SSP, 1, 1);
   Chip_SSP_Enable(LPC_SSP);

}

void AD9833setFrequency(long frequency, int Waveform)
{
   long FreqWord = (frequency * pow(2, 28)) / 25.0E6;
   int MSB = (int)((FreqWord & 0xFFFC000) >> 14);
   int LSB = (int)(FreqWord & 0x3FFF);
   LSB |= 0x4000;
   MSB |= 0x4000;
   WriteRegister(0x2100);
   WriteRegister(LSB);
   WriteRegister(MSB);
   WriteRegister(0xC000);
   WriteRegister(Waveform);
}

int main(void)
{	 int i=0;
	SystemCoreClockUpdate();

	Chip_GPIO_Init(LPC_GPIO_PORT);

	Chip_SCU_PinMuxSet(0x1, 5, (SCU_PINIO_FAST | SCU_MODE_FUNC5));  /* P1.5 => SSEL1 */
	Chip_SCU_PinMuxSet(0xF, 4, (SCU_PINIO_FAST | SCU_MODE_FUNC0));  /* PF.4 => SCK1 */

	Chip_SCU_PinMuxSet(0x1, 4, (SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS | SCU_MODE_FUNC5)); /* P1.4 => MOSI1 */
	Chip_SCU_PinMuxSet(0x1, 3, (SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS | SCU_MODE_FUNC5)); /* P1.3 => MISO1 */

	Chip_SCU_PinMuxSet(0x3, 1, (SCU_MODE_INACT | SCU_MODE_FUNC4));  /* P1.5 => SSEL1 */
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 8);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 8, (bool) true);

	Chip_SSP_Init(LPC_SSP);
	Chip_SSP_SetFormat(LPC_SSP, SSP_BITS_8, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_MODE2);
	Chip_SSP_Set_Mode(LPC_SSP, SSP_MODE_MASTER);
	Chip_SSP_SetClockRate(LPC_SSP, 1, 1);
	Chip_SSP_Enable(LPC_SSP);



	SysTick_Config(SystemCoreClock / TICKRATE_HZ);	//204.000 ticks entre interrupciones -> interrumpe cada 100ms

	function = 0x2002;
	counter = 900;

	Chip_SCU_PinMuxSet(2, 10, SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS); //LED1
		Chip_SCU_PinMuxSet(2, 11, SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS); //LED2
		Chip_SCU_PinMuxSet(2, 12, SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS); //LED3
		Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 0, 14); //LED1
		Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 1, 11); //LED2
		Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 1, 12); //LED3
		Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) false); //LED1
			Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool) false); //LED2
			Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool) false); //LED3

	//AD9833setFrequency(counter, function);


	while(1)
	{
		Chip_GPIO_SetPinToggle(LPC_GPIO_PORT, 0, 14);

		while(tick_ct < 3000);
		tick_ct = 0;

		Chip_GPIO_SetPinToggle(LPC_GPIO_PORT, 1, 11);

		while(tick_ct < 3000);
		tick_ct = 0;

		Chip_GPIO_SetPinToggle(LPC_GPIO_PORT, 1, 12);

		while(tick_ct < 3000);
		tick_ct = 0;

		WriteRegister(0x100);
		for(i=0;i<1000;i++);



	}
    return 0 ;
}

