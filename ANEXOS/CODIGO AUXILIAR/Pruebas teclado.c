/*
===============================================================================
 Name        : Prueba_ILI9341.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "chip.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tm_stm32f4_ili9341.h"
#include "tm_stm32f4_fonts.h"
#include <math.h>

#define LPC_SSP    LPC_SSP1
#define WAVE_SINE     0x2000
#define WAVE_SQUARE   0x2028
#define WAVE_TRIANGLE 0x2002
#define SIZE_ARRAY   5

int i , p = 0;
long int counter = 9000;
long int function = 0;

bool encoder_CLK_last = false;
bool encoder_CLK;
int encoder_position=0 , a=0 , b=0 ;

//------------------TECLADO---------------
int F_port[] = {5,5,5,5};
int F_pin[]  = {4,3,8,9};
int C_port[] = {3,2,2,2};
int C_pin[]  = {12,0,3,2};

char frecuencia[] = {'0','0','0','0','0', 0};
char teclas[4][4] = {{'1','2','3','A'},
                     {'4','5','6','B'},
                     {'7','8','9','C'},
                     {'*','0','#','D'}};
//-----------------------------------------

int teclado(void)
{
    TM_ILI9341_Init();
    TM_ILI9341_Puts(0, 0,  "Frecuencia:[Hz]", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);
    int c = 0, stop_flag = 0, valor_frecuencia = 0;
    char user_char;
    //flag para ver si termina de ingresar
    while(!stop_flag) {
        //Barrido por las filas
        for (int nL = 0; !stop_flag && nL <= 3; nL++) {
            //limpiar el pin de cada celda
            Chip_GPIO_SetPinState(LPC_GPIO_PORT, F_port[nL], F_pin[nL], (bool) false);
            //Barrido en columnas buscando un LOW
            for (int nC = 0; !stop_flag && nC <= 3; nC++) {
                if (Chip_GPIO_GetPinState(LPC_GPIO_PORT, C_port[nC],C_pin[nC]) == 0) {
                    user_char = teclas[nL][nC];
                    //checkeamos que la letra ingresada no sea B
                    if(user_char == 'B'){
                        stop_flag = 1;
                        break;
                    }
                    //si el user se pasa, borrar el numero y resetear contador
                    if(c > SIZE_ARRAY){
                        c = 0;
                        valor_frecuencia = 10000;
                    }
                    //ingresamos la letra al buffer
                    frecuencia[c] = user_char;
_                   //traspasa ascii a numero
                    for(int i=0; i < SIZE_ARRAY; i++) valor_frecuencia += (user_char - '0') * pow(10, SIZE_ARRAY-c-1);
                    //imprime el input en pantalla
                    TM_ILI9341_Puts(0, 20, frecuencia, &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);
                    while(Chip_GPIO_GetPinState(LPC_GPIO_PORT, C_port[nC],C_pin[nC]) == 0){} //loop
                    c++;
                }
            }
            Chip_GPIO_SetPinState(LPC_GPIO_PORT, F_port[nL], F_pin[nL], (bool) true);
            }
    }
    return valor_frecuencia;
}

int nav (void)
    {
        bool encoder_CLK = Chip_GPIO_GetPinState(LPC_GPIO_PORT, 3, 5);
        int e = 0;

                if(encoder_CLK != encoder_CLK_last)
                {
                    if(Chip_GPIO_GetPinState(LPC_GPIO_PORT, 5, 16) != encoder_CLK ) //encoder_DT
                    {
                        encoder_position++;
                    }
                    else
                    {
                        encoder_position--;
                    }
                }
                encoder_CLK_last = encoder_CLK;

                e = encoder_position;

                if( e<0 )
                {e = 4; encoder_position = 4;}

    }

void AD9833Config(void)
    {

    Chip_GPIO_Init(LPC_GPIO_PORT);

    //--------SPI

    Chip_SCU_PinMuxSet(0x1, 5, (SCU_PINIO_FAST | SCU_MODE_FUNC5));  /* P1.5 => SSEL1 */
    Chip_SCU_PinMuxSet(0xF, 4, (SCU_PINIO_FAST | SCU_MODE_FUNC0));  /* PF.4 => SCK1 */

    Chip_SCU_PinMuxSet(0x1, 4, (SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS | SCU_MODE_FUNC5)); /* P1.4 => MOSI1 */
    Chip_SCU_PinMuxSet(0x1, 3, (SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS | SCU_MODE_FUNC5)); /* P1.3 => MISO1 */

    Chip_SSP_Init(LPC_SSP);
    Chip_SSP_SetFormat(LPC_SSP, SSP_BITS_16, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_MODE2);
    Chip_SSP_Set_Mode(LPC_SSP, SSP_MODE_MASTER);
    Chip_SSP_SetBitRate(LPC_SSP, 3906250);
    Chip_SSP_Enable(LPC_SSP);
    }

void Write (uint16_t dat)
{
    Chip_SSP_SendFrame(LPC_SSP, dat);
}

void AD9833setFrequency(long frequency, int Waveform)
{
   long FreqWord = (frequency * pow(2, 28)) / 25.0E6;
   int MSB = (int)((FreqWord & 0xFFFC000) >> 14);
   int LSB = (int)(FreqWord & 0x3FFF);
   LSB |= 0x4000;
   MSB |= 0x4000;
   Write(0x2100);
   Write(LSB);
   Write(MSB);
   Write(0xC000);
   Write(Waveform);
}


int main(void)
{
        Chip_GPIO_Init(LPC_GPIO_PORT);

        Chip_SCU_PinMux( 6 , 9 ,  SCU_MODE_PULLUP  | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );//encoder_CLK
        Chip_SCU_PinMux( 6 , 10 , SCU_MODE_PULLUP  | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );//encoder_SW
        Chip_SCU_PinMux( 6 , 8 , SCU_MODE_PULLUP  | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );//encoder_DT

        Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 3, 5); //encoder_CLK
        Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 3, 6); //encoder_SW
        Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 5, 16); //encoder_DT

        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 0, 14); //LED1
        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 1, 11); //LED2
        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 1, 12); //LED3

        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) false); //LED1
        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool) false); //LED2
        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool) false); //LED3

        Chip_SCU_PinMux( 2 , 0 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//LED0_R
        Chip_SCU_PinMux( 2 , 1 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//LED0_G
        Chip_SCU_PinMux( 2 , 2 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//LED0_B

        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 0); //LED0_R
        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 1); //LED0_G
        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 2); //LED0_B

        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 0, (bool) false); //LED0_R
        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 1, (bool) false); //LED0_G
        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) false); //LED0_B

//---------------Teclado
        Chip_SCU_PinMux( 2 , 4 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//F1
        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 4); //F1
        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 4, (bool) true); //F1

        Chip_SCU_PinMux( 2 , 3 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//F2
        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 3); //F2
        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 3, (bool) true); //F2

        Chip_SCU_PinMux( 3 , 1 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//F3
        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 8); //F3
        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 8, (bool) true); //F3

        Chip_SCU_PinMux( 3 , 2 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//F4
        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 9); //F4
        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 9, (bool) true); //F4

        Chip_SCU_PinMux( 7 , 4 , SCU_MODE_PULLUP  | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );//encoder_C1
        Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 3, 12); //C1

        Chip_SCU_PinMux( 4 , 0 , SCU_MODE_PULLUP  | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );//encoder_C2
        Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 2, 0); //C2

        Chip_SCU_PinMux( 4 , 3 , SCU_MODE_PULLUP  | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );//encoder_C3
        Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 2, 3); //C3

        Chip_SCU_PinMux( 4 , 2 , SCU_MODE_PULLUP  | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );//encoder_C4
        Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 2, 2); //C4
//---------------------------------------

        encoder_CLK_last = Chip_GPIO_GetPinState(LPC_GPIO_PORT, 3, 5);

    TM_ILI9341_Init();

    TM_ILI9341_Puts(0, 0,  "[ ]SIN", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE   );
    TM_ILI9341_Puts(0, 107,"[ ]TRI", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE   );
    TM_ILI9341_Puts(0, 213,"[ ]SQU", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE   );

    while(1)
    {

        a = nav();

                if(a == 0)
                {
                    Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool) false);
                    Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool) false);
                    Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) true);

                    b=0;
                }

                if(a == 1 || a == 2)
                {
                    Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) false);
                    Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool) false);
                    Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool)  true);
                    b=1;
                }

                if(a == 3 || a == 4)
                {
                    Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) false);
                    Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool) false);
                    Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool)  true);
                    b=2;
                }

                if( a>5 )
                {encoder_position = 0;}


                  if(Chip_GPIO_GetPinState(LPC_GPIO_PORT, 3, 6) == 0)
               {
                switch(b)
                {
                    case 0:
                        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 0, (bool) true); //LED0_R
                        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 1, (bool) false); //LED0_G
                        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) false); //LED0_B

                        TM_ILI9341_Init();
                        TM_ILI9341_Puts(0, 0,  "[*]SIN", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE   );
                        TM_ILI9341_Puts(0, 107,"[ ]TRI", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE   );
                        TM_ILI9341_Puts(0, 213,"[ ]SQU", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE   );

                        p = teclado();
                        AD9833Config();
                        function = WAVE_SINE;
                        AD9833setFrequency(counter, function);

                        break;

                    case 1:
                        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 0, (bool) false); //LED0_R
                        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 1, (bool) false); //LED0_G
                        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) true); //LED0_B

                        TM_ILI9341_Init();
                        TM_ILI9341_Puts(0, 0,  "[ ]SIN", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE   );
                        TM_ILI9341_Puts(0, 107,"[*]TRI", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE   );
                        TM_ILI9341_Puts(0, 213,"[ ]SQU", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE   );

                        AD9833Config();
                        function = WAVE_TRIANGLE;
                        AD9833setFrequency(counter, function);

                        break;

                    case 2:

                        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 0, (bool) false); //LED0_R
                        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 1, (bool) true); //LED0_G
                        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) false); //LED0_B

                        TM_ILI9341_Init();
                        TM_ILI9341_Puts(0, 0,  "[ ]SIN", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE   );
                        TM_ILI9341_Puts(0, 107,"[ ]TRI", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE   );
                        TM_ILI9341_Puts(0, 213,"[*]SQU", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE   );

                        AD9833Config();
                        function = WAVE_SQUARE;
                        AD9833setFrequency(counter, function);

                        break;
                }
            }

    }
    return 0 ;
}
