/*
===============================================================================
 Name        : Prueba_ENCODER.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/
#include "chip.h"
bool encoder_CLK_last = false;
bool encoder_CLK;
int encoder_position=0 , a=0 , b=0 ;

void GPIO0_IRQHandler(void)
{

}

int main(void)
{
	SystemCoreClockUpdate();
	Chip_GPIO_Init(LPC_GPIO_PORT);
	Chip_SCU_PinMux( 6 , 9 ,  SCU_MODE_PULLUP  | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );//encoder_CLK
	Chip_SCU_PinMux( 6 , 10 , SCU_MODE_PULLUP  | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );//encoder_SW
	Chip_SCU_PinMux( 6 , 8 , SCU_MODE_PULLUP  | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );//encoder_DT

	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 3, 5); //encoder_CLK
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 3, 6); //encoder_SW
	Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 5, 16); //encoder_DT

	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 0, 14); //LED1
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 1, 11); //LED2
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 1, 12); //LED3

	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) false); //LED1
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool) false); //LED2
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool) false); //LED3

	Chip_SCU_PinMux( 2 , 0 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//LED0_R
	Chip_SCU_PinMux( 2 , 1 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//LED0_G
	Chip_SCU_PinMux( 2 , 2 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//LED0_B

	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 0); //LED0_R
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 1); //LED0_G
	Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 2); //LED0_B

	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 0, (bool) false); //LED0_R
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 1, (bool) false); //LED0_G
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) false); //LED0_B

	encoder_CLK_last = Chip_GPIO_GetPinState(LPC_GPIO_PORT, 3, 5);

	NVIC_ClearPendingIRQ(PIN_INT0_IRQn );
	NVIC_EnableIRQ(PIN_INT0_IRQn );

	while(1)
	{

		bool encoder_CLK = Chip_GPIO_GetPinState(LPC_GPIO_PORT, 3, 5);

		if(encoder_CLK != encoder_CLK_last)
		{
			if(Chip_GPIO_GetPinState(LPC_GPIO_PORT, 5, 16) != encoder_CLK ) //encoder_DT
			{
				encoder_position++;
			}
			else
			{
				encoder_position--;
			}
		}
		encoder_CLK_last = encoder_CLK;

		a = encoder_position;

		if( a<0 )
		{a = 4; encoder_position = 4;}
		if(a == 0)
		{
			Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool) false);
			Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool) false);
			Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) true);

			b=0;
		}

		if(a == 1 || a == 2)
		{
			Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) false);
			Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool) false);
			Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool)  true);

			b=1;
		}

		if(a == 3 || a == 4)
		{
			Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) false);
			Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool) false);
			Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool)  true);

			b=2;
		}

		if( a>5 )
		{encoder_position = 0;}


		  if(Chip_GPIO_GetPinState(LPC_GPIO_PORT, 3, 6) == 0)
	   {
		switch(b)
		{
			case 0:
				Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 0, (bool) true); //LED0_R
				Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 1, (bool) false); //LED0_G
				Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) false); //LED0_B
					break;
			case 1:
				Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 0, (bool) false); //LED0_R
			    Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 1, (bool) false); //LED0_G
			    Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) true); //LED0_B
					break;
			case 2:

				Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 0, (bool) false); //LED0_R
				Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 1, (bool) true); //LED0_G
				Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) false); //LED0_B
					break;
		}
	}


	}


}
