/*
===============================================================================
 Name        : Prueba_ILI9341.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#include "chip.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tm_stm32f4_ili9341.h"
#include "tm_stm32f4_fonts.h"
#include <math.h>

//---------Menu
#define cant_opciones_menu 5
bool flag_back = 0; //flag para retroceder en los menues


//----------ADS---------------
#define LPC_SSP    LPC_SSP1
#define WAVE_SINE     0x2000
#define WAVE_SQUARE   0x2028
#define WAVE_TRIANGLE 0x2002
#define SIZE_ARRAY   7

int i , p = 0;
long int counter = 9000;
long int function = 0;

//---------ENCODER----------------------
bool encoder_CLK_last = false;
bool encoder_CLK;
int  a=0 , b=0 ;
float encoder_position = 0;

//------------------TECLADO---------------
#define tecla_confirmar 'A'
#define tecla_borrar 'B'
#define pos_x_0 0
#define pos_x_1 16
#define pos_x_2 32
#define pos_x_3 100
#define pos_x_4 120

int F_port[] = {5,5,5,5};
int F_pin[]  = {4,3,8,9};
int C_port[] = {3,2,2,2};
int C_pin[]  = {12,0,3,2};

char frecuencia[] = {'0','0','0','0','0','0','0', 0};
char teclas[4][4] = {{'1','2','3','A'},
                     {'4','5','6','B'},
                     {'7','8','9','C'},
                     {'*','0','#','D'}};
//----------------DAC----------------------

#define WaveSize	 	    7	 // Cantidad de muestras de la tabla
#define SIZE_TABLA			7	//Cantidad de muestras
#define sinFrec	    		2000 //Frecuencia

#define PRIMER_CUADRANTE	15
#define SEGUNDO_CUADRANTE	30
#define TERCER_CUADRANTE	45
#define CUARTO_CUADRANTE	60

DMA_TransferDescriptor_t dmaDacTransfer;

static volatile uint32_t dacCountSmp;
static volatile uint8_t channelTC, dmaChannelNum;

uint16_t seno_0_a_90 [16] = {0,1045,2079,3090,4067,5000,5877,6691,7431,8090,8660,9135,9510,9781,9945,10000};

uint16_t SineWave [WaveSize] ;

char Tabla_Muestas[SIZE_TABLA] = {0,0,0,0,0,0,0};

uint32_t DMAbuffer;

//-----------------------------------------
/* DMA routine for DAC example */
static void App_DMA_Test(void)
{
	uint32_t tmp = 0;
	//volatile uint32_t i = 0;

	/* Initialize GPDMA controller */
	Chip_GPDMA_Init(LPC_GPDMA);
	/* Setting GPDMA interrupt */
	NVIC_DisableIRQ(DMA_IRQn);
	NVIC_SetPriority(DMA_IRQn, ((0x01 << 3) | 0x01));
	NVIC_EnableIRQ(DMA_IRQn);
	/* Get the free channel for DMA transfer */
	dmaChannelNum = Chip_GPDMA_GetFreeChannel(LPC_GPDMA, GPDMA_CONN_DAC);

	while(Chip_GPIO_GetPinState(LPC_GPIO_PORT, 3, 6) != 0) //al apretar el boton del encoder salimos de la generacion
	{
		/* Start D/A conversion */
		tmp++;
		if (tmp == (SIZE_TABLA - 1)) {
			tmp = 0;
		}
		/* pre-format the data to DACR register */
		DMAbuffer = (uint32_t) (DAC_VALUE(Tabla_Muestas[tmp]) | DAC_BIAS_EN);

		channelTC = 0;
		Chip_GPDMA_Transfer(LPC_GPDMA, dmaChannelNum,
						  (uint32_t) &DMAbuffer,
						  GPDMA_CONN_DAC,
						  GPDMA_TRANSFERTYPE_M2P_CONTROLLER_DMA,
						  1);

		/* Waiting for writing DAC value completed */
		while (channelTC == 0) {}
	}
	/* Disable interrupts, release DMA channel */
	Chip_GPDMA_Stop(LPC_GPDMA, dmaChannelNum);
	NVIC_DisableIRQ(DMA_IRQn);
}

void DMA_IRQHandler(void) // Rutina de atencion a la tinerrupcion del DMA
{
	if (Chip_GPDMA_Interrupt(LPC_GPDMA, dmaChannelNum) == SUCCESS) //Chequeamos que
	{
		channelTC++;
	}
	else {
		/* Process error here */
	}
}


void Init (void)
{
			Chip_GPIO_Init(LPC_GPIO_PORT);

			//--------ENCODER-----------
	        Chip_SCU_PinMux( 6 , 9 ,  SCU_MODE_PULLUP  | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );//encoder_CLK
	        Chip_SCU_PinMux( 6 , 10 , SCU_MODE_PULLUP  | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );//encoder_SW
	        Chip_SCU_PinMux( 6 , 8 , SCU_MODE_PULLUP  | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );//encoder_DT

	        Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 3, 5); //encoder_CLK
	        Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 3, 6); //encoder_SW
	        Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 5, 16); //encoder_DT

	        //-----------LEDS-----------
	        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 0, 14); //LED1
	        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 1, 11); //LED2
	        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 1, 12); //LED3

	        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) false); //LED1
	        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool) false); //LED2
	        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool) false); //LED3

	        Chip_SCU_PinMux( 2 , 0 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//LED0_R
	        Chip_SCU_PinMux( 2 , 1 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//LED0_G
	        Chip_SCU_PinMux( 2 , 2 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//LED0_B

	        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 0); //LED0_R
	        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 1); //LED0_G
	        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 2); //LED0_B

	        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 0, (bool) false); //LED0_R
	        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 1, (bool) false); //LED0_G
	        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) false); //LED0_B

			//---------------Teclado------------
			// F = filas del teclado matricial
			// C = columnas del teclado matricial
	        Chip_SCU_PinMux( 2 , 4 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//F1
	        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 4); //F1
	        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 4, (bool) true); //F1

	        Chip_SCU_PinMux( 2 , 3 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//F2
	        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 3); //F2
	        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 3, (bool) true); //F2

	        Chip_SCU_PinMux( 3 , 1 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//F3
	        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 8); //F3
	        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 8, (bool) true); //F3

	        Chip_SCU_PinMux( 3 , 2 , SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC4 );//F4
	        Chip_GPIO_SetPinDIROutput(LPC_GPIO_PORT, 5, 9); //F4
	        Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 9, (bool) true); //F4

	        Chip_SCU_PinMux( 7 , 4 , SCU_MODE_PULLUP  | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );//C1
	        Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 3, 12); //C1

	        Chip_SCU_PinMux( 4 , 0 , SCU_MODE_PULLUP  | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );//C2
	        Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 2, 0); //C2

	        Chip_SCU_PinMux( 4 , 3 , SCU_MODE_PULLUP  | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );//C3
	        Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 2, 3); //C3

	        Chip_SCU_PinMux( 4 , 2 , SCU_MODE_PULLUP  | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS , SCU_MODE_FUNC0 );//C4
			Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, 2, 2); //C4

			//-------------------DAC------------------
	        Chip_SCU_DAC_Analog_Config(); //configuramos el pin del DAC
			Chip_DAC_Init(LPC_DAC);// inicializamos el periferico DAC
			dacCountSmp = Chip_Clock_GetRate(CLK_APB3_DAC)/ (sinFrec * WaveSize); //tiempo de actualizacion de la salida
			Chip_DAC_SetDMATimeOut(LPC_DAC, dacCountSmp);	//Seteo cada cuanto tiempo actualizo la salida.
			Chip_DAC_ConfigDAConverterControl(LPC_DAC, (DAC_CNT_ENA | DAC_DMA_ENA)); //Habilitamos el contador de timeout y DMA

}

int Teclado(void)
{
    //Limpio el valor de frecuencia
    for(int i=0;i<SIZE_ARRAY;i++){
    frecuencia[i] = '0';
   }
    frecuencia[SIZE_ARRAY] = 0;

    TM_ILI9341_Init();
    Imprimir_Menu();

    int c = 0 , stop_flag = 0, borrar_flag = 0, valor_frecuencia = 0;
    memset(frecuencia, 0, SIZE_ARRAY);
    char user_char;
    //flag para ver si termina de ingresar
    while(!stop_flag) {
        //Barrido por las filas
        for (int nL = 0; !stop_flag && nL <= 3; nL++) {
            //limpiar el pin de cada celda
            Chip_GPIO_SetPinState(LPC_GPIO_PORT, F_port[nL], F_pin[nL], (bool) false);
            //Barrido en columnas buscando un LOW
            for (int nC = 0; !stop_flag && nC <= 3; nC++) {
                if (Chip_GPIO_GetPinState(LPC_GPIO_PORT, C_port[nC],C_pin[nC]) == 0) {
                	//tomamos la tecla ingresada
                    user_char = teclas[nL][nC];

                    //checkeamos que la letra ingresada no sea la de confirmacion
                    if(user_char == tecla_confirmar){
                        stop_flag = 1;
                        break;
                    }
                    //chequeamos si desea borrar
                    if(user_char == tecla_borrar){
                    	borrar_flag = 1;
                    	frecuencia[c] = '0';
                    }
                    else{
                        //ingresamos la letra al buffer
                        frecuencia[c] = user_char;
                    }


                    //imprime el input en pantalla
                   TM_ILI9341_Puts(pos_x_2, 40, frecuencia, &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);

                    while(Chip_GPIO_GetPinState(LPC_GPIO_PORT, C_port[nC],C_pin[nC]) == 0){} //loop

                    if(borrar_flag == 1 && c !=0){
                    	c--;
                    	borrar_flag = 0;
                    	}
                    else{
                    	c++;
                    }
                }
            }
            Chip_GPIO_SetPinState(LPC_GPIO_PORT, F_port[nL], F_pin[nL], (bool) true);
            }
        //si el user se pasa, borrar el numero y resetear contador
		if(c >= SIZE_ARRAY)
		{
			c = 0;
			c= SIZE_ARRAY-1;
			valor_frecuencia = 0;
		}
        //Hago titilar el dígito donde va el próximo numero a ingresar
        TM_ILI9341_Puts(pos_x_2 + c*16, 40, "_", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);
        TM_ILI9341_Puts(pos_x_2 + c*16, 40, " ", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);



    }
    Imprimir_Menu();
    //traspasa ascii a numero
	for(i=0; i < SIZE_ARRAY; i++) {
		if(frecuencia[i] ){
		valor_frecuencia += (frecuencia[i] - '0') * pow(10, SIZE_ARRAY-i-1);
		}
	}
    return valor_frecuencia;
}

void Nav (void)
    {
        bool encoder_CLK = Chip_GPIO_GetPinState(LPC_GPIO_PORT, 3, 5);

                if(encoder_CLK != encoder_CLK_last)
                {
                    if(Chip_GPIO_GetPinState(LPC_GPIO_PORT, 5, 16) != encoder_CLK ) //encoder_DT
                    {
                        encoder_position+= 0.5;
                    }
                    else
                    {
                        encoder_position-= 0.5;
                    }
                }
                encoder_CLK_last = encoder_CLK;


                if( encoder_position < 0 )
                {
                 encoder_position = cant_opciones_menu - 1;
                }

                if( encoder_position > cant_opciones_menu + 1 )
                {
                	encoder_position = 0;
                }

    }

void AD9833Config(void)
    {

    Chip_GPIO_Init(LPC_GPIO_PORT);

    //--------SPI

    Chip_SCU_PinMuxSet(0x1, 5, (SCU_PINIO_FAST | SCU_MODE_FUNC5));  /* P1.5 => SSEL1 */
    Chip_SCU_PinMuxSet(0xF, 4, (SCU_PINIO_FAST | SCU_MODE_FUNC0));  /* PF.4 => SCK1 */

    Chip_SCU_PinMuxSet(0x1, 4, (SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS | SCU_MODE_FUNC5)); /* P1.4 => MOSI1 */
    Chip_SCU_PinMuxSet(0x1, 3, (SCU_MODE_INACT | SCU_MODE_INBUFF_EN | SCU_MODE_ZIF_DIS | SCU_MODE_FUNC5)); /* P1.3 => MISO1 */

    Chip_SSP_Init(LPC_SSP);
    Chip_SSP_SetFormat(LPC_SSP, SSP_BITS_16, SSP_FRAMEFORMAT_SPI, SSP_CLOCK_MODE2);
    Chip_SSP_Set_Mode(LPC_SSP, SSP_MODE_MASTER);
    Chip_SSP_SetBitRate(LPC_SSP, 3906250);
    Chip_SSP_Enable(LPC_SSP);
    }

void Write (uint16_t dat) //unificarcon ads9833
{
    Chip_SSP_SendFrame(LPC_SSP, dat);
}

void AD9833setFrequency(long frequency, int Waveform)
{
   long FreqWord = (frequency * pow(2, 28)) / 25.0E6;
   int MSB = (int)((FreqWord & 0xFFFC000) >> 14);
   int LSB = (int)(FreqWord & 0x3FFF);
   LSB |= 0x4000;
   MSB |= 0x4000;
   Write(0x2100);
   Write(LSB);
   Write(MSB);
   Write(0xC000);
   Write(Waveform);
}

void Imprimir_Menu(void)
{

    TM_ILI9341_Puts(pos_x_1, 0,  "Frec:", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);
    TM_ILI9341_Puts(pos_x_2, 40, "0000000Hz", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);
    TM_ILI9341_Puts(pos_x_2, 40, frecuencia, &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);
    TM_ILI9341_Puts(pos_x_1, 80, "SIN", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE   );
    TM_ILI9341_Puts(pos_x_1, 120, "TRI", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE   );
    TM_ILI9341_Puts(pos_x_1, 200,"SQU", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE   );
    TM_ILI9341_Puts(pos_x_1, 240,"DAC", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE   );
}

void Imprimir_Menu_Dac(void)

{
	TM_ILI9341_Puts(pos_x_1, 0,  "Menu DAC", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);
	TM_ILI9341_Puts(pos_x_1, 40,  "Cantidad de muestras 10 (0 a 3.3V)", &TM_Font_7x10,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);
	TM_ILI9341_Puts(pos_x_1, 80, "M =  v", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);
	TM_ILI9341_Puts(pos_x_4, 80, "Save", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);
	TM_ILI9341_Puts(pos_x_4, 120, "Saved", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);
}

void Almacenar_Tabla_DAC(void)
{
	 //Limpio la tabla de muestras
	    for(int i=0;i<SIZE_TABLA;i++){
	    	Tabla_Muestas[i] =  0;
	   }
	    //frecuencia[SIZE_ARRAY] = 0;

	    //TM_ILI9341_Init();
	    //Imprimir_Menu();

	    int c = 0 , stop_flag = 0, borrar_flag = 0, valor_frecuencia = 0;
	    memset(frecuencia, 0, SIZE_ARRAY);
	    char user_char;
	    //flag para ver si termina de ingresar

	    while(!stop_flag) {
	        //Barrido por las filas del teclado
	        for (int nL = 0; !stop_flag && nL <= 3; nL++) {
	            //limpiar el pin de cada celda
	            Chip_GPIO_SetPinState(LPC_GPIO_PORT, F_port[nL], F_pin[nL], (bool) false);
	            //Barrido en columnas del teclado buscando un 0
	            for (int nC = 0; !stop_flag && nC <= 3; nC++) {
	                if (Chip_GPIO_GetPinState(LPC_GPIO_PORT, C_port[nC],C_pin[nC]) == 0) {
	                	//tomamos la tecla ingresada
	                    user_char = teclas[nL][nC];

	                    //checkeamos que la letra ingresada no sea la de confirmacion
	                    if(user_char == tecla_confirmar){
	                        stop_flag = 1;
	                        break;
	                    }
	                    //chequeamos si desea borrar
	                    if(user_char == tecla_borrar){
	                    	borrar_flag = 1;
	                    	Tabla_Muestas[c] = 0;
	                    }
	                    else{
	                        //ingresamos la tecla al buffer
	                    	Tabla_Muestas[c] = user_char ;
	                    }

	                    //imprime el input en pantalla
	                   TM_ILI9341_Puts(pos_x_2, 200, Tabla_Muestas, &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);

	                    while(Chip_GPIO_GetPinState(LPC_GPIO_PORT, C_port[nC],C_pin[nC]) == 0){} //loop

	                    if(borrar_flag == 1 && c !=0){
	                    	c--;
	                    	borrar_flag = 0;
	                    	}
	                    else{
	                    	c++;
	                    }
	                }
	            }
	            Chip_GPIO_SetPinState(LPC_GPIO_PORT, F_port[nL], F_pin[nL], (bool) true);
	            }
	        //si el user se pasa, borrar el numero y resetear contador
			if(c >= SIZE_TABLA)
			{
				c = 0;
				c= SIZE_TABLA-1;
				valor_frecuencia = 0;
			}
	        //Hago titilar el dígito donde va el próximo numero a ingresar
	        TM_ILI9341_Puts(pos_x_2 + c*16, 200, "_", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);
	        TM_ILI9341_Puts(pos_x_2 + c*16, 200, " ", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);
	    }
	    //Imprimir_Menu();

	    //traspasa ascii a numero
		for(i=0; i < SIZE_TABLA; i++) {
			if(Tabla_Muestas[i] ){
			Tabla_Muestas[i] = (Tabla_Muestas[i] - '0')*100;
			}
		}
	    //return valor_frecuencia;
}


int main(void)
{
		Init();
        encoder_CLK_last = Chip_GPIO_GetPinState(LPC_GPIO_PORT, 3, 5);

    	AD9833Config();
    	Write(0x100);

    	TM_ILI9341_Init();
        Imprimir_Menu();


    while(1)
    {
    		Nav();

                if(encoder_position == 0)
                {
                    Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool) false);
                    Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool) false);
                    Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) true);

                    TM_ILI9341_Puts(pos_x_0, 0, ">", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);
                    TM_ILI9341_Puts(pos_x_0, 0, " ", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);

                    if(Chip_GPIO_GetPinState(LPC_GPIO_PORT, 3, 6) == 0)
                    {
                     //Muestro cual seleccioné
                     TM_ILI9341_Puts(pos_x_1, 0,  "Frec:", &TM_Font_16x26,ILI9341_COLOR_WHITE,  ILI9341_COLOR_BLACK);
                     counter = Teclado();
                    }

                }

                if(encoder_position == 1 )
                {
                     if(Chip_GPIO_GetPinState(LPC_GPIO_PORT, 3, 6) == 0)
                    {
                     TM_ILI9341_Puts(pos_x_1, 80, "SIN", &TM_Font_16x26,ILI9341_COLOR_WHITE,  ILI9341_COLOR_BLACK);

					 AD9833Config();
					 Write(0x100);
					 function = WAVE_SINE;
                     AD9833setFrequency(counter, function);
                     for(int u=0;u<10000;u++){}
                     TM_ILI9341_Init();
                     Imprimir_Menu();

                    }
                     Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) false);
					 Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool) false);
					 Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool)  true);

					 TM_ILI9341_Puts(pos_x_0, 80, ">", &TM_Font_16x26,ILI9341_COLOR_BLACK,ILI9341_COLOR_WHITE);
					 TM_ILI9341_Puts(pos_x_0, 80, " ", &TM_Font_16x26,ILI9341_COLOR_BLACK,ILI9341_COLOR_WHITE);

                }

                if(encoder_position == 2)
                {
                    Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) false);
                    Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool) false);
                    Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool)  true);

                    TM_ILI9341_Puts(pos_x_0, 120, ">", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE   );
                    TM_ILI9341_Puts(pos_x_0, 120, " ", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE   );

                    if(Chip_GPIO_GetPinState(LPC_GPIO_PORT, 3, 6) == 0)
                    {
                     TM_ILI9341_Puts(pos_x_1, 120, "TRI", &TM_Font_16x26,ILI9341_COLOR_WHITE,  ILI9341_COLOR_BLACK);

                     AD9833Config();
                     Write(0x100);
                     function = WAVE_TRIANGLE;
                     AD9833setFrequency(counter, function);
                     for(int u=0;u<10000;u++){}
                     TM_ILI9341_Init();
                     Imprimir_Menu();
                     }

                }
                if(encoder_position == 3)
                {
                    TM_ILI9341_Puts(pos_x_0, 200, ">", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);
                    TM_ILI9341_Puts(pos_x_0, 200, " ", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);

                    if(Chip_GPIO_GetPinState(LPC_GPIO_PORT, 3, 6) == 0)
                    {
                        TM_ILI9341_Puts(pos_x_1, 200,"SQU", &TM_Font_16x26,ILI9341_COLOR_WHITE, ILI9341_COLOR_BLACK);

                     AD9833Config();
                     Write(0x100);
                     function = WAVE_SQUARE;
                     AD9833setFrequency(counter, function);
                     for(int u=0;u<10000;u++){}
                     TM_ILI9341_Init();
                     Imprimir_Menu();
                     }

                }
                if(encoder_position ==4 )
                {
                    TM_ILI9341_Puts(pos_x_0, 240, ">", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);
                    TM_ILI9341_Puts(pos_x_0, 240, " ", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);

                    if(Chip_GPIO_GetPinState(LPC_GPIO_PORT, 3, 6) == 0)
                    {
                    	TM_ILI9341_Puts(pos_x_1, 240,"DAC", &TM_Font_16x26,ILI9341_COLOR_WHITE,  ILI9341_COLOR_BLACK);
                    	TM_ILI9341_Fill(ILI9341_COLOR_WHITE);
                    	Imprimir_Menu_Dac();

                    	while(flag_back !=1)
                    	{
                    		Nav();
                    		switch((int)encoder_position)
							{
                    		case 1:
                    		TM_ILI9341_Puts(pos_x_0, 80, ">", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);
                    		TM_ILI9341_Puts(pos_x_0, 80, " ", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);

                    		if(Chip_GPIO_GetPinState(LPC_GPIO_PORT, 3, 6) == 0) // si entro en esa opcion almacena la tabla y la saca por el dac
                    		{
                    		Almacenar_Tabla_DAC();
                    		App_DMA_Test(); //Por ahora si entra no sale, esto es un while 1
                    		flag_back = 1;
							}
                    			break;
                    		case 2:
                    		TM_ILI9341_Puts(pos_x_3, 80, ">", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);
                    		TM_ILI9341_Puts(pos_x_3, 80, " ", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);

                    			break;
                    		case 3:
							TM_ILI9341_Puts(pos_x_3, 120, ">", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);
							TM_ILI9341_Puts(pos_x_3, 120, " ", &TM_Font_16x26,ILI9341_COLOR_BLACK,  ILI9341_COLOR_WHITE);

                    			break;
                    		default:
                    			encoder_position = 1;
                    			break;

                    		}
                    	}
                    	flag_back = 0;

                    //Si entro genero la senoidal y la saco por el dac
            		/*	for(int i = 0 ; i<WaveSize ; i++ )
            			{
            				if(i <= PRIMER_CUADRANTE) //0 a 90
            				{
            					SineWave[i] = 512 + 512 * seno_0_a_90 [i] /10000;
            					if( i == PRIMER_CUADRANTE)
            						SineWave[i]=1023;
            				}
            				else
            					if(i <= SEGUNDO_CUADRANTE)
            						SineWave[i]= 512 + 512 * seno_0_a_90 [SEGUNDO_CUADRANTE - i] /10000;
            					else
            						if(i <= TERCER_CUADRANTE)
            							SineWave[i]= 512 - 512 * seno_0_a_90 [i - SEGUNDO_CUADRANTE ] /10000;
            						else
            							SineWave[i]= 512 - 512 * seno_0_a_90 [ CUARTO_CUADRANTE - i] /10000;

            				//SineWave [i] = (SineWave [i] << 6);
            			}*/


                     TM_ILI9341_Init();
                     Imprimir_Menu();
                     }
                }



    }
    return 0 ;
}
